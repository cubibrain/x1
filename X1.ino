
#include "MPU6050_tockn.h"
#include <Wire.h>

static const int test = 0;
static const int mpuCount = 3;
static const int addressLength = 4;

MPU6050 mpu[mpuCount] = {
  MPU6050(Wire), MPU6050(Wire), MPU6050(Wire)
};

float offset [3][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
float error [3][3] = {{9.19, -1.55, -13.33}, {-2.88, -4.36, -1.94}, {12.40, -4.06, 9.09}};
int buzzer = 11;

void setDigital(int pin, int val) {
  pinMode(pin, OUTPUT);
  digitalWrite(pin, val);
}

void printAngles(int address) {
  Serial.print(-mpu[address].getAngleX() + offset[address][0]);Serial.print("|");
  Serial.print(-mpu[address].getAngleY() + offset[address][1]);Serial.print("|");
  Serial.print(-mpu[address].getAngleZ() + offset[address][2]);
  if (address < mpuCount - 1)
    Serial.print("|");
  else
    Serial.println("");
}

void switchAddress(int address) {
  for (int b = 0; b < addressLength; b++) {
    int pin = (b + 1) * 2;
    int val = address & 1 << b ? HIGH : LOW;
    setDigital(pin, val);
    setDigital(pin + 1, val);
  //    setAddressBit(b, val);
  }
}

void calibrate() {
  Serial.println("Calibrating...");
  long measureCount = 9999;
  for (long m = 0; m < measureCount; m++) {
    for (long i = 0; i < mpuCount; i++) {
      offset[i][0] += mpu[i].getAngleX();
      offset[i][1] += mpu[i].getAngleY();
      offset[i][2] += mpu[i].getAngleZ();
    }
  }

  for (int i = 0; i < mpuCount; i++) {
    offset[i][0] /= measureCount;
    offset[i][1] /= measureCount;
    offset[i][2] /= measureCount;
  }

  for (int i = 0; i < mpuCount; i++) {
    for (int j = 0; j < 3; j++) {
      offset[i][j] -= error[i][j];
    }
  }
  Serial.println("Calibrate finished");
}

const int c = 261;
const int d = 294;
const int e = 329;
const int f = 349;
const int g = 391;
const int gS = 415;
const int a = 440;
const int aS = 455;
const int b = 466;
const int cH = 523;
const int cSH = 554;
const int dH = 587;
const int dSH = 622;
const int eH = 659;
const int fH = 698;
const int fSH = 740;
const int gH = 784;
const int gSH = 830;
const int aH = 880;
 
 
void beep(int ton, int time)
{
  tone(buzzer, ton, time);
  delay(time + 20);
}

void imperialMarch() {
    beep(a, 500);
    beep(a, 500);
    beep(a, 500);
    beep(f, 350);
    beep(cH, 150);  
    beep(a, 500);
    beep(f, 350);
    beep(cH, 150);
    beep(a, 650);
 
    delay(150);
    //end of first bit   
 
    beep(eH, 500);
    beep(eH, 500);
    beep(eH, 500);   
    beep(fH, 350);
    beep(cH, 150);
    beep(gS, 500);
    beep(f, 350);
    beep(cH, 150);
    beep(a, 650);
 
    delay(150);
    //end of second bit...  
 
    beep(aH, 500);
    beep(a, 300);
    beep(a, 150);
    beep(aH, 400);
    beep(gSH, 200);
    beep(gH, 200); 
    beep(fSH, 125);
    beep(fH, 125);    
    beep(fSH, 250);
 
    delay(250);
 
    beep(aS, 250); 
    beep(dSH, 400); 
    beep(dH, 200);  
    beep(cSH, 200);  
    beep(cH, 125);  
    beep(b, 125);  
    beep(cH, 250);  
 
    delay(250);
 
    beep(f, 125);  
    beep(gS, 500);  
    beep(f, 375);  
    beep(a, 125);
    beep(cH, 500);
    beep(a, 375);  
    beep(cH, 125);
    beep(eH, 650);
 
    //end of third bit... (Though it doesn't play well)
    //let's repeat it
 
    beep(aH, 500);
    beep(a, 300);
    beep(a, 150);
    beep(aH, 400);
    beep(gSH, 200);
    beep(gH, 200);
    beep(fSH, 125);
    beep(fH, 125);    
    beep(fSH, 250);
 
    delay(250);
 
    beep(aS, 250);  
    beep(dSH, 400);  
    beep(dH, 200);  
    beep(cSH, 200);  
    beep(cH, 125);  
    beep(b, 125);  
    beep(cH, 250);      
 
    delay(250);
 
    beep(f, 250);  
    beep(gS, 500);  
    beep(f, 375);  
    beep(cH, 125);
    beep(a, 500);   
    beep(f, 375);   
    beep(cH, 125); 
    beep(a, 650);   
    //end of the song
    
    noTone(buzzer);
}

void beepInit() {
  beep(b, 200);
}

void beepCalibrated() {
  beep(b, 200);
  beep(a, 250);
}

void setup() {
  Serial.begin(38400);
  Serial.println("Started");
  noTone(buzzer);
//  imperialMarch();
  
  if (test == 1) {
    Serial.println("Test");
    return;
  }
  
  Wire.begin();

  for (int i = 0; i < mpuCount; i++) {
    switchAddress(i);
    mpu[i].begin();
//    arr[i].calcGyroOffsets(true);
  }

  beepInit();
  calibrate();
  beepCalibrated();
}


long timer = 1;

void loop() {
  if(millis() - timer < 10)
    return;

  if (test == 1) {
    return;
  }

  for (int i = 0; i < mpuCount; i++) {
    switchAddress(i);
    mpu[i].update();
    printAngles(i);
  }

  timer = millis();
}



